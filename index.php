<?php
session_start();
include_once('contenu.php');

?>
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Fragola Games</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="style/style.css" rel="stylesheet" type="text/css">
<link href="style/style_carte.css" rel="stylesheet" type="text/css">
<script src="js/script_carte.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

</head>
<body>
<div id="menu"><div id="en_menu"><?php include_once("menu/menu.inc.php"); ?></div></div>
<div id="head"></div>
<div id="blocpage">
	
		<div id="fragola_resultat"></div>
		<?php contenu(); ?>
	
	
		
</div>

</body>
</html>